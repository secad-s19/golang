/**** package bitbucket.org/secad-s19/golang/jsondb/users
* For Assignment 2 - Part II - Database with JSON 
* In "Secure Application Development" - Spring 2019 - UDayton
* Author: Phu H. Phung
* License: You can use this package within your application
* without any warantty 
*/
package users

import (
	"os"
	"fmt"
	"encoding/json"
	"io/ioutil"
	"errors"
	"golang.org/x/crypto/pbkdf2" //go get golang.org/x/crypto/pbkdf2 //set $GOPATH
	"crypto/sha256"
	"encoding/hex"
)

type UsersDB struct{
	Users []Account 
}

type Account struct{
	Username string 
	Password string //stored as the hashed value of plaintext password 
}
var usersDB UsersDB
var username string
var password string

func init() {
	fmt.Println("package bitbucket.org/secad-s19/golang/jsondb/users is invoked")
}

func ListAllAccounts() {
	OpenUsersDB()
	for i := 0; i < len(usersDB.Users); i++ {
		fmt.Printf ("Username[%d]:%s;Password=%s\n",i, usersDB.Users[i].Username,usersDB.Users[i].Password)
		
	}
	
}
func RemoveAccount(username string) error {
	if !validateUsername(username) {
		return errors.New("RemoveAccount: username is invalid.")
	}
	OpenUsersDB()
	if !ExistAccount(username){
		return errors.New("No account to remove: " + username)
	}
	for i := 0; i < len(usersDB.Users); i++{
		if usersDB.Users[i].Username == username {
			// Remove the element at index i from usersDB.Users
			usersDB.Users[i] = usersDB.Users[len(usersDB.Users)-1] // Copy last element to index i.
			usersDB.Users[len(usersDB.Users)-1] = Account{}   // Clear the content of the last element 
			usersDB.Users = usersDB.Users[:len(usersDB.Users)-1]   // Truncate slice.
		}
	}
	write_err := UpdateJsonDB()
	if write_err !=nil {
		return write_err		
	}
	return nil	
}

func AddAccount(username string, password string) error {
	if !validateUsername(username) || !validatePassword(password){
		return errors.New("AddAccount: username or password is invalid.")
	}
	OpenUsersDB()
	if ExistAccount(username){
		return errors.New("AddAccount: Account exists: " + username)
	}
	usersDB.Users = append(usersDB.Users, Account{Username: username, Password: hash(password,username)})
	write_err := UpdateJsonDB()
	if write_err !=nil {
		return write_err		
	}
	return nil	
}

func UpdateAccount(username string, password string) error {
	if !validateUsername(username) || !validatePassword(password){
		return errors.New("UpdateAccount: username or password is invalid.")
	}
	OpenUsersDB()
	if !ExistAccount(username){
		return errors.New("UpdateAccount: Account does not exist!")
	}
	for i := 0; i < len(usersDB.Users); i++ {
		if (usersDB.Users[i].Username == username) {
			usersDB.Users[i].Password = hash(password,username)
			break
		}
		
	}
	update_err := UpdateJsonDB()
	if update_err !=nil {
		return update_err		
	}
	return nil	
}

func CheckAccount(username string, password string) bool {
	if !validateUsername(username) || !validatePassword(password){
		return false
	}
	OpenUsersDB()
	for i := 0; i < len(usersDB.Users); i++ {
		if username == usersDB.Users[i].Username && hash(password,username) == usersDB.Users[i].Password{
			return true
		}		
	}
	return false	
}

func CheckAccountHashed(username string, password string) bool {
	if !validateUsername(username) || !validatePassword(password){
		return false
	}
	OpenUsersDB()
	for i := 0; i < len(usersDB.Users); i++ {
		if username == usersDB.Users[i].Username && password == usersDB.Users[i].Password{
			return true
		}		
	}
	return false	
}
func ExistAccount(username string) bool {
	if !validateUsername(username) {
		return false
	}
	OpenUsersDB()
	for i := 0; i < len(usersDB.Users); i++ {
		if username == usersDB.Users[i].Username{
			return true
		}		
	}
	return false
}
func OpenUsersDB() {
	
	filepointer, open_err := os.Open("users-database.json")
	defer filepointer.Close()
	if open_err != nil {
		fmt.Println("No users-database.json file. Need to add a new user to create one.")
		json.Unmarshal([]byte(`{"users":[]}`), & usersDB)	
	} else {
		jsondatabase, read_err := ioutil.ReadAll(filepointer)
		if read_err != nil {
			fmt.Println("Error in reading the JSON data")
			os.Exit(1)
		}
		unmarshal_err := json.Unmarshal(jsondatabase, & usersDB)
		if unmarshal_err != nil {
			fmt.Println("Error in mapping the JSON data")
			os.Exit(1)
		}
	}	
	
}
func UpdateJsonDB() error {
	if usersDB.Users == nil {
		json.Unmarshal([]byte(`{"users":[]}`), & usersDB)
	}
	jsondatabase, ind_err := json.MarshalIndent(usersDB, "", " ")
	if ind_err != nil {
		return errors.New("Cannot MarshalIndent JSON object")
	}
	write_err := ioutil.WriteFile("users-database.json", jsondatabase, 0644)
	if write_err != nil {
		return write_err	
	}
	fmt.Println("Write successfully to users-database.json")
	return nil
}

func hash(data string, salt string) string {
	//hash the data using pbkdf2 algorithm with sha256 to generate 32-byte hashed value 
	//that can be used as a key or symetric key encrytption and description 
	//
	hashed_data := hex.EncodeToString(pbkdf2.Key([]byte(data), []byte(salt), 4096, 32, sha256.New))
	return hashed_data	
}
func validateUsername(username string) bool {
	//ensure that username is not null or too short or too long, 
	
	if username == "" || len(username) < 5 || len(username) > 15{
		return false
	}
	//TODO: ensure that it follows a username rule, 
	//e.g., only characters, numbers, ..
	return true
	
}

func validatePassword(password string) bool {
	//ensure that username is not null or too short or too long
	if password == "" || len(password) < 5 || len(password) > 100{
		return false
	}
	//TODO: ensure that it follows a password rule, 
	//e.g., must have uppercase, lowercase, special characters, numbers, ..
	return true
	
}

